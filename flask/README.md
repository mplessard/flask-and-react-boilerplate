# Boilerplate

This project is a flask boilerplate

## Dependencies
- python3.6
- pip3.6
- sqlite

## How to

1. Create virtualenv
    ```
    python3.6 -m venv boilerplate-env
    ```
2. Activate virtualenv  
    ```
    source boilerplate-venv/bin/activate
    ```
3. Install requirements  
    ```
    pip3.6 install -r requirements.txt
    ```
4. Export env variables
    ```
    export FLASK_ENV=development
    export FLASK_APP=manager.py
    ```
5. Create database (if there is already a migrations directory, then just follow the last line.)
    ```
    mkdir api/database
    flask db init
    flask db migrate -m "initialize database"
    flask db upgrade # if migrations directory already exist only execute this line
    ```
6. Run app
    ```
    flask run
    ```

