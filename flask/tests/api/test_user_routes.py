import json
import unittest
from api import create_app, db, API_PREFIX, models


class TestUserAPI(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.db = db
        self.db.create_all()
        self.client = self.app.test_client(use_cookies=True)

        self.SAMPLE_USERS = {
            'Jon': {
                'first_name': 'Jon',
                'last_name': 'Snow',
                'password': '1KingOfTheNorth',
                'email': 'jon.snow@winterfell.com',
                'remember_me': True
            },
            'Sansa': {
                'first_name': 'Sansa',
                'last_name': 'Stark',
                'password': 'LadyOfWinterfell2',
                'email': 'sansa.stark@winterfell.com',
                'remember_me': True
            }
        }

    def tearDown(self):
        self.db.session.remove()
        self.db.drop_all()
        self.app_context.pop()

    def test_WhenIPostWithAnInvalidEmail_ThenItShouldReturnAnError(self):
        user = self.SAMPLE_USERS.get('Jon')
        user['email'] = 'invalid_email'

        response = self.client.post(f'{API_PREFIX}/user', data=user)
        data = json.loads(response.get_data())

        self.assertEquals(400, response.status_code)
        self.assertTrue('email' in data.get('message'))

    def test_WhenIPostWithAnAlreadyExistingEmail_ThenItShouldReturnAnError(self):
        self.create_user('Jon')

        response = self.client.post(f'{API_PREFIX}/user', data=self.SAMPLE_USERS.get('Jon'))
        data = json.loads(response.get_data())

        self.assertEquals(400, response.status_code)
        self.assertTrue('email' in data.get('message'))

    def test_WhenIPostWithASmallPassword_ThenItShouldReturnAnError(self):
        user = self.SAMPLE_USERS.get('Jon')
        user['password'] = 'a1'

        response = self.client.post(f'{API_PREFIX}/user', data=user)
        data = json.loads(response.get_data())

        self.assertEquals(400, response.status_code)
        self.assertTrue('password' in data.get('message'))

    def test_WhenIPostWithAPasswordWithoutALetter_ThenItShouldReturnAnError(self):
        user = self.SAMPLE_USERS.get('Jon')
        user['password'] = '12345678'

        response = self.client.post(f'{API_PREFIX}/user', data=user)
        data = json.loads(response.get_data())

        self.assertEquals(400, response.status_code)
        self.assertTrue('password' in data.get('message'))

    def test_WhenIPostWithAPasswordWithoutANumber_ThenItShouldReturnAnError(self):
        user = self.SAMPLE_USERS.get('Jon')
        user['password'] = 'KingOfTheNorth'

        response = self.client.post(f'{API_PREFIX}/user', data=user)
        data = json.loads(response.get_data())

        self.assertEquals(400, response.status_code)
        self.assertTrue('password' in data.get('message'))

    def test_WhenIPostCorrectlyToUserEndpoint_ThenItShouldReturnAnAccessToken(self):
        self.create_user()


    def test_WhenIPostWithoutAFirstName_ThenItShouldReturnAnError(self):
        user = self.SAMPLE_USERS.get('Jon')
        del user['first_name']

        response = self.client.post(f'{API_PREFIX}/user', data=user)
        data = json.loads(response.get_data())

        self.assertEquals(400, response.status_code)
        self.assertTrue('first_name' in data.get('message'))

    def test_WhenIPostWithoutALastName_ThenItShouldReturnAnError(self):
        user = self.SAMPLE_USERS.get('Jon')
        del user['last_name']

        response = self.client.post(f'{API_PREFIX}/user', data=user)
        data = json.loads(response.get_data())

        self.assertEquals(400, response.status_code)
        self.assertTrue('last_name' in data.get('message'))

    def test_WhenIPostWithoutAPassword_ThenItShouldReturnAnError(self):
        user = self.SAMPLE_USERS.get('Jon')
        del user['password']

        response = self.client.post(f'{API_PREFIX}/user', data=user)
        data = json.loads(response.get_data())

        self.assertEquals(400, response.status_code)
        self.assertTrue('password' in data.get('message'))

    def test_WhenIPostWithoutAnEmail_ThenItShouldReturnAnError(self):
        user = self.SAMPLE_USERS.get('Jon')
        del user['email']

        response = self.client.post(f'{API_PREFIX}/user', data=user)
        data = json.loads(response.get_data())

        self.assertEquals(400, response.status_code)
        self.assertTrue('email' in data.get('message'))

    def test_WhenIPutMyOwnUserCorrectly_ThenIShouldGetASuccess(self):
        data = self.create_user()
        user_id = data.get('user_id')
        access_token = data.get('access_token')

        user = {
            'first_name': 'Aegon',
            'last_name': 'Targaryen',
            'email': 'aegon.targaryen@dragonstone.com'
        }

        headers = {'Authorization': f'Bearer {access_token}'}

        response = self.client.put(f'{API_PREFIX}/user/{user_id}', data=user, headers=headers)
        data = json.loads(response.get_data())

        self.assertEquals(200, response.status_code)
        self.assertTrue('success' in data.get('message'))

        db_user = models.User.query.get(user_id)

        self.assertEquals(user.get('first_name'), db_user.first_name)
        self.assertEquals(user.get('last_name'), db_user.last_name)
        self.assertEquals(user.get('email'), db_user.email)

    def test_WhenIPutAnotherUserWithoutHavingTheRequiredAccesses_ThenIShouldGetADeniedAccess(self):
        user = self.create_user()
        access_token = user.get('access_token')

        user_update = {
            'first_name': 'Aegon',
            'last_name': 'Targaryen',
            'email': 'aegon.targaryen@dragonstone.com'
        }

        headers = {'Authorization': f'Bearer {access_token}'}

        response = self.client.put(f'{API_PREFIX}/user/100', data=user_update, headers=headers)

        data = json.loads(response.get_data())

        self.assertEquals(401, response.status_code)
        self.assertTrue('admin' in data.get('message'))

    def test_WhenIPutAUserWithoutBeingAuthenticated_ThenIShouldGetADeniedAccess(self):
        data = self.create_user()
        user_id = data.get('user_id')

        user_update = {
            'first_name': 'Aegon',
            'last_name': 'Targaryen',
            'email': 'aegon.targaryen@dragonstone.com'
        }

        response = self.client.put(f'{API_PREFIX}/user/{user_id}', data=user_update)
        data = json.loads(response.get_data())

        self.assertEquals(401, response.status_code)
        self.assertTrue('Missing Authorization Header' in data.get('message'))

    def test_WhenIPutANonExistingUserAsAnAdmin_ThenIShouldGetError(self):
        data = self.create_user()
        user = models.User.query.get(data.get('user_id'))
        user.role = models.UserRole.ADMIN
        self.db.session.commit()

        response = self.client.post(f'{API_PREFIX}/login', data=self.SAMPLE_USERS.get('Jon'))
        data = json.loads(response.get_data())
        access_token = data.get('access_token')

        self.assertEquals(200, response.status_code)
        self.assertTrue(access_token is not None)

        user_update = {
            'first_name': 'Aegon',
            'last_name': 'Targaryen',
            'email': 'aegon.targaryen@dragonstone.com'
        }

        headers = {'Authorization': f'Bearer {access_token}'}

        response = self.client.put(f'{API_PREFIX}/user/100', data=user_update, headers=headers)
        data = json.loads(response.get_data())

        self.assertEquals(400, response.status_code)
        self.assertTrue('user_id' in data.get('message'))

    def test_WhenIPutAnExistingUserAsAnAdmin_ThenIShouldGetASuccess(self):
        data_to_modify = self.create_user('Sansa')
        user_id = data_to_modify.get('user_id')
        data = self.create_user()
        user = models.User.query.get(data.get('user_id'))
        user.role = models.UserRole.ADMIN
        self.db.session.commit()

        response = self.client.post(f'{API_PREFIX}/login', data=self.SAMPLE_USERS.get('Jon'))
        data = json.loads(response.get_data())
        access_token = data.get('access_token')

        self.assertEquals(200, response.status_code)
        self.assertTrue(access_token is not None)

        user_update = {
            'first_name': 'Aegon',
            'last_name': 'Targaryen',
            'email': 'aegon.targaryen@dragonstone.com'
        }

        headers = {'Authorization': f'Bearer {access_token}'}

        response = self.client.put(f'{API_PREFIX}/user/{user_id}', data=user_update, headers=headers)
        data = json.loads(response.get_data())

        self.assertEquals(200, response.status_code)
        self.assertTrue('success' in data.get('message'))

    def test_WhenIGetAnotherUserWithAuthentication_ThenIShouldGetTheInformation(self):
        data = self.create_user()
        user_id = data.get('user_id')

        data = self.create_user('Sansa')
        access_token = data.get('access_token')

        headers = {'Authorization': f'Bearer {access_token}'}

        response = self.client.get(f'{API_PREFIX}/user/{user_id}', headers=headers)
        data = json.loads(response.get_data())

        self.assertEquals(200, response.status_code)
        self.assertTrue(data.get('first_name'))
        self.assertTrue(data.get('last_name'))

    def test_WhenIGetANonExistingUser_ThenIShouldGetAnError(self):
        data = self.create_user()
        access_token = data.get('access_token')

        headers = {'Authorization': f'Bearer {access_token}'}

        response = self.client.get(f'{API_PREFIX}/user/100', headers=headers)
        data = json.loads(response.get_data())

        self.assertEquals(400, response.status_code)
        self.assertTrue('user_id' in data.get('message'))

    def test_WhenIGetAUserWithoutBeingAuthenticated_ThenIShouldGetAnError(self):
        response = self.client.get(f'{API_PREFIX}/user/100')
        data = json.loads(response.get_data())

        self.assertEquals(401, response.status_code)
        self.assertTrue('Missing Authorization Header' in data.get('message'))

    def test_WhenILoginWithRightInformation_ThenIShouldGetAccessToken(self):
        self.create_user()

        response = self.client.post(f'{API_PREFIX}/login', data=self.SAMPLE_USERS.get('Jon'))
        data = json.loads(response.get_data())

        self.assertEquals(200, response.status_code)
        self.assertTrue(data.get('access_token') is not None)
        self.assertTrue(data.get('refresh_token') is not None)

    def test_WhenILoginWithRightInformationWithoutRememberMe_ThenIShouldGetAccessTokenWithoutRefreshToken(self):
        self.create_user()
        user = self.SAMPLE_USERS.get('Jon')
        del user['remember_me']

        response = self.client.post(f'{API_PREFIX}/login', data=user)
        data = json.loads(response.get_data())

        self.assertEquals(200, response.status_code)
        self.assertTrue(data.get('access_token') is not None)
        self.assertTrue(data.get('refresh_token') is None)

    def test_WhenILoginWithWrongPassword_ThenIShouldGetAnError(self):
        self.create_user()
        user = self.SAMPLE_USERS.get('Jon')
        user['password'] = 'wrongPassword'

        response = self.client.post(f'{API_PREFIX}/login', data=user)
        data = json.loads(response.get_data())

        self.assertEquals(401, response.status_code)
        self.assertTrue('login' in data.get('message'))

    def test_WhenILoginWithUnexistingEmail_ThenIShouldGetAnError(self):
        response = self.client.post(f'{API_PREFIX}/login', data=self.SAMPLE_USERS.get('Jon'))
        data = json.loads(response.get_data())

        self.assertEquals(401, response.status_code)
        self.assertTrue('login' in data.get('message'))

    def create_user(self, user='Jon'):
        response = self.client.post(f'{API_PREFIX}/user', data=self.SAMPLE_USERS[user])
        data = json.loads(response.get_data())

        self.assertEquals(201, response.status_code)
        self.assertTrue(data.get('access_token') is not None)
        self.assertTrue(data.get('user_id') is not None)

        return data

    def test_WhenILogout_ThenIShouldGetASuccessAndTokenShouldBeRevoked(self):
        data = self.create_user()
        access_token = data.get('access_token')

        headers = {'Authorization': f'Bearer {access_token}'}

        response = self.client.post(f'{API_PREFIX}/logout', headers=headers)
        data = json.loads(response.get_data())

        self.assertEquals(200, response.status_code)
        self.assertTrue('Access token has been revoked' in data.get('message'))

        response = self.client.get(f'{API_PREFIX}/user/1', headers=headers)
        data = json.loads(response.get_data())

        self.assertEquals(401, response.status_code)
        self.assertTrue('Token has been revoked' in data.get('message'))

    # TODO refresh token

    # TODO allow password reset when admin
