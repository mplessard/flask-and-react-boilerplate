import os
import binascii

SQLITE_PATH = 'sqlite:///'
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    TESTING = False

    SECRET_KEY = os.environ.get('SECRET_KEY') or binascii.hexlify(os.urandom(24))
    JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY') or binascii.hexlify(os.urandom(24))
    JWT_BLACKLIST_ENABLED = True

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True

    BUNDLE_ERRORS = True

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or os.path.join(f'{SQLITE_PATH}{BASE_DIR}', 'database/dev.db')


class TestingConfig(Config):
    TESTING = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or os.path.join(f'{SQLITE_PATH}{BASE_DIR}', 'database/test.db')


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or os.path.join(f'{SQLITE_PATH}{BASE_DIR}', 'database/prod.db')


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig,
}
