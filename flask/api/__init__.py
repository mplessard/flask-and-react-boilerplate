from flask import Flask
from flask_jwt_extended import JWTManager
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

from api.config import config

API_PREFIX='/v1'

db = SQLAlchemy()
jwt = JWTManager()
api = Api(prefix=API_PREFIX)


def create_app(config_name='default'):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    db.init_app(app)
    jwt.init_app(app)

    from api import models
    from api.user import routes

    api.init_app(app)
    CORS(app)

    return app
