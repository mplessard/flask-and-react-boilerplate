from marshmallow import Schema, fields


class UserSchema(Schema):
    class Meta:
        ordered = True

    id = fields.Integer()
    first_name = fields.Str()
    last_name = fields.Str()
    middle_name = fields.Str()
    sign_in_date = fields.DateTime()
