from flask_jwt_extended import create_access_token, create_refresh_token, get_raw_jwt
from flask_jwt_extended import jwt_required, jwt_refresh_token_required
from flask_restful import Resource, abort

from api import auth, models
from api.user import service, routes_models, schemas
from api import api, db
from api.auth import admin_or_owner_required, revoke_token


class UserAPI(Resource):
    def post(self):
        args = routes_models.get_post_user_arguments()

        service.validate_new_user_information(args)

        args['salt'] = auth.generate_salt()
        args['password'] = auth.hash_password(args.get('password'), args.get('salt'))

        user = models.User(**args)

        db.session.add(user)
        db.session.commit()

        access_token = create_access_token(identity=user.id)

        return {'access_token': access_token, 'user_id': user.id}, 201

    @admin_or_owner_required
    def put(self, user_id):
        args = routes_models.get_put_user_arguments()

        service.validate_update_user_information(args, user_id)

        user = models.User.query.get(user_id)
        new_password = args.get('new_password')

        if new_password is not None:
            args['salt'] = auth.generate_salt()
            args['password'] = auth.hash_password(new_password, args.get('salt'))

            del args['actual_password']
            del args['new_password']

        for key, value in args.items():
            setattr(user, key, value)

        db.session.commit()

        return {'message': 'success'}, 200

    @jwt_required
    def get(self, user_id):
        user = models.User.query.get(user_id)

        if not user:
            abort(400, message={'user_id': 'user_id is invalid.'})

        return schemas.UserSchema().dump(user).data


api.add_resource(UserAPI, '/user/<int:user_id>', '/user', endpoint='user')


class UserLoginAPI(Resource):
    def post(self):
        args = routes_models.get_post_user_login_arguments()
        user = service.validate_login_user_information(args)
        remember_me = args.get('remember_me')

        access_token = create_access_token(identity=user.id)
        data = {'access_token': access_token}

        if remember_me:
            data['refresh_token'] = create_refresh_token(identity=user.id)

        return data


api.add_resource(UserLoginAPI, '/login', endpoint='login')


class UserLogoutAPI(Resource):
    @jwt_required
    def post(self):
        revoke_token(get_raw_jwt())
        return {'message': 'Access token has been revoked'}, 200


api.add_resource(UserLogoutAPI, '/logout', endpoint='logout')


class UserLogoutRefreshAPI(Resource):
    @jwt_refresh_token_required
    def post(self):
        revoke_token(get_raw_jwt())
        return {'message': 'Refresh token has been revoked'}, 200


api.add_resource(UserLogoutRefreshAPI, '/logout/refresh', endpoint='logout/refresh')


class UserRecoverPassword(Resource):
    def post(self):
        args = routes_models.get_post_user_recover_password()
        user = service.validate_recover_password_information(args)

        # TODO send code to user's email
