from flask_restful import abort

from api import models


def validate_new_user_information(args):
    email = args.get('email')
    password = args.get('password')

    if not models.User.is_valid_email(email):
        abort(400, message={'email': 'Email is invalid.'})
    if models.User.is_existing_email(email):
        abort(400, message={'email': 'Email already exists.'})
    if not models.User.is_valid_password(password):
        abort(400, message={'password': f'Password must be at least {models.User.MIN_LENGTH_PASSWORD} alphanumeric characters, 1 letter and 1 number.'})


def validate_update_user_information(args, user_id):
    email = args.get('email')
    new_password = args.get('new_password')
    actual_password = args.get('actual_password')

    if not models.User.is_valid_user_id(user_id):
        abort(400, message={'user_id': 'user_id is invalid.'})
    if email and not models.User.is_valid_email(email):
        abort(400, message={'email': 'email is invalid.'})
    if email and models.User.is_existing_email(email):
        abort(400, message={'email': 'email already exists.'})
    if new_password and not models.User.is_user_password(user_id, actual_password):
        abort(400, message={'password': 'actual password is not valid.'})


def validate_login_user_information(args):
    email = args.get('email')
    password = args.get('password')

    user = models.User.query.filter_by(email=email).first()

    if user is None:
        abort(401, message={'login': 'invalid email or password.'})
    if not models.User.is_user_password(user.id, password):
        abort(401, message={'login': 'invalid email or password.'})

    return user


def validate_recover_password_information(args):
    email = args.get('email')
    user = models.User.query.filter_by(email=email).first()

    if user is None:
        abort(400, message={'email': 'invalid email'})

    return user
