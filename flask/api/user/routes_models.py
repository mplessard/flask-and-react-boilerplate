from flask_restful import reqparse
from datetime import datetime
from api import models

def get_post_user_arguments():
    user_parser = reqparse.RequestParser()
    user_parser.add_argument('first_name', type=str, required=True, help='User\'s firstname. Required.')
    user_parser.add_argument('last_name', type=str, required=True, help='User\'s lastname. Required.')
    user_parser.add_argument('password', type=str, required=True, help='User\'s password. Required.')
    user_parser.add_argument('email', type=str, required=True, help='User\'s email address. Required.')
    user_parser.add_argument('middle_name', type=str, required=False, help='User\'s middle name.')
    user_parser.add_argument('birth_date', type=datetime, required=False, help='User\'s birthday.')
    user_parser.add_argument('phone_number', type=str, required=False, help='User\'s phone number.')

    return user_parser.parse_args()


def get_put_user_arguments():
    user_parser = reqparse.RequestParser()
    user_parser.add_argument('first_name', type=str, help='User\'s firstname.')
    user_parser.add_argument('last_name', type=str, help='User\'s lastname.')
    user_parser.add_argument('actual_password', type=str, help='User\'s actual password.')
    user_parser.add_argument('new_password', type=str, help='User\'s new password.')
    user_parser.add_argument('email', type=str, help='User\'s email address.')
    user_parser.add_argument('middle_name', type=str, help='User\'s middle name.')
    user_parser.add_argument('birth_date', type=datetime, help='User\'s birthday.')
    user_parser.add_argument('phone_number', type=str, help='User\'s phone number.')

    return user_parser.parse_args()


def get_post_user_login_arguments():
    user_parser = reqparse.RequestParser()
    user_parser.add_argument('email', type=str, required=True, help='User\'s email address. Required.')
    user_parser.add_argument('password', type=str, required=True, help='User\'s password. Required.')
    user_parser.add_argument('remember_me', type=bool, required=False, help='User\'s wants to stay logged in?')

    return user_parser.parse_args()

def get_post_user_recover_password():
    user_parser = reqparse.RequestParser()
    user_parser.add_argument('email', type=str, required=True, help='User\'s email address. Required.')
