import hashlib
import uuid
from functools import wraps

from flask import jsonify
from flask_jwt_extended import verify_jwt_in_request, get_jwt_claims

from api import jwt, models, db


def generate_salt():
    return uuid.uuid4().hex


def hash_password(password, salt):
    password_and_salt = f'{password}{salt}'.encode('utf-8')
    return hashlib.sha512(password_and_salt).hexdigest()


@jwt.unauthorized_loader
def api_unauthorized_loader(message):
    return jsonify({'message': message}), 401


@jwt.expired_token_loader
def api_expired_token_loader():
    return api_unauthorized_loader('Token has expired')


@jwt.invalid_token_loader
def api_invalid_token_loader(message):
    return jsonify({'message': message}), 422


@jwt.needs_fresh_token_loader
def api_needs_fresh_token_loader():
    return api_unauthorized_loader('Fresh token required')


@jwt.revoked_token_loader
def api_revoked_token_loader():
    return api_unauthorized_loader('Token has been revoked')


@jwt.user_loader_error_loader
def api_user_loader_error_loader(identity):
    return api_unauthorized_loader('User not found')


@jwt.token_in_blacklist_loader
def api_token_in_blacklist_loader(token):
    jti = token.get('jti')
    return models.RevokedToken.is_jti_blacklisted(jti)


def admin_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        jwt.verify_jwt_in_request()
        claims = jwt.get_jwt_claims()

        if claims['role'] != 'admin':
            return {'message': 'admin only'}, 403
        else:
            return f(*args, **kwargs)

    return wrapper


def admin_or_owner_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()

        user_id = kwargs.get('user_id')

        if claims.get('role') == models.UserRole.ADMIN:
            return f(*args, **kwargs)
        if user_id and user_id == claims.get('identity'):
            return f(*args, **kwargs)
        return {'message': 'admin only'}, 401

    return wrapper


@jwt.user_claims_loader
def api_add_claims_to_access_token(identity):
    user = models.User.query.filter_by(id=identity).first()
    if user:
        return {'role': user.role,
                'identity': identity}
    else:
        return {'role': 'guest'}

@jwt.user_loader_callback_loader
def api_user_loader_callback(identity):
    return models.User.query.filter_by(id=identity).first()


def revoke_token(token):
    jti = token.get('jti')
    revoked_token = models.RevokedToken(jti=jti)

    db.session.add(revoked_token)
    db.session.commit()
