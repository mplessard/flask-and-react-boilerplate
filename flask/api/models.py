import enum
import datetime
import re

from api import db, auth


class UserRole(enum.IntEnum):
    ADMIN = 1
    MODERATOR = 2
    USER = 3


class User(db.Model):
    __tablename__ = 'user'

    MIN_LENGTH_PASSWORD = 8

    id = db.Column(db.Integer, primary_key=True)
    role = db.Column(db.Enum(UserRole), default=UserRole.USER, nullable=False)
    first_name = db.Column(db.String(50), nullable=False)
    middle_name = db.Column(db.String(50))
    last_name = db.Column(db.String(50), nullable=False)
    birth_date = db.Column(db.Date)
    sign_up_date = db.Column(db.DateTime, default=datetime.datetime.now(), nullable=False)
    sign_in_date = db.Column(db.DateTime, default=datetime.datetime.now(), nullable=False)
    phone_number = db.Column(db.String(25))
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(150), nullable=False)
    salt = db.Column(db.String(40), nullable=False)

    def __repr__(self):
        return f'<User email:{self.email} role:{self.role}>'

    @classmethod
    def is_existing_email(cls, email):
        return bool(User.query.filter_by(email=email).first())

    @classmethod
    def is_valid_email(cls, email):
        if email is None:
            return False
        if not re.match('[a-zA-Z0-9\._-]*@[a-zA-Z]*(\.[a-zA-Z]*)+', email):
            return False
        return True

    @classmethod
    def is_valid_password(cls, password):
        if password is None:
            return False
        if len(password) < cls.MIN_LENGTH_PASSWORD:
            return False
        if not re.search('[0-9]', password):
            return False
        if not re.search('[a-zA-Z]', password):
            return False
        return True

    @classmethod
    def is_valid_user_id(cls, user_id):
        if user_id is None:
            return False
        if cls.query.get(user_id) is None:
            return False
        return True

    @classmethod
    def is_user_password(cls, user_id, password):
        user = cls.query.get(user_id)
        if password is None:
            return False
        if user is None:
            return False
        if user.password != auth.hash_password(password, user.salt):
            return False
        return True


class RevokedToken(db.Model):
    __tablename__ = 'revoked_token'
    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(120))

    @classmethod
    def is_jti_blacklisted(cls, jti):
        return bool(cls.query.filter_by(jti=jti).first())
