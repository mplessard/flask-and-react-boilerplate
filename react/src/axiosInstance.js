import Cookies from 'universal-cookie';

const cookies = new Cookies();
const axios = require('axios');

const axiosInstance = axios.create({
    baseURL: 'http://127.0.0.1:5000/v1'
});

if (cookies.get('jwt')) {
  axiosInstance.defaults.headers.common.Authorization = `Bearer ${cookies.get('jwt')}`;
}

export default axiosInstance;
