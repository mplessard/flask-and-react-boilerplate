import React from 'react';

export const InputError = ({error}) => (
  <div className="small input-error text-danger px-1">
      { error }
  </div>
);