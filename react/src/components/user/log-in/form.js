import React from 'react';
import {
  Form,
  Button,
  FormGroup,
  Label,
  Input,
  InputGroupAddon,
  InputGroup
} from 'reactstrap';
import Cookies from 'universal-cookie';
import { withRouter } from 'react-router-dom';

import '../../../static/css/login.scss';
import { InputError } from '../../form/input-error';
import axios from '../../../axiosInstance';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
    }
  }

  onFieldChange = (event) => {
    if (event.target.type === 'checkbox') {
      this.setState({
        [event.target.name]: event.target.checked
      });
    } else {
      this.setState({
        [event.target.name]: event.target.value
      });
    }
  };

  onClick = () => {
    axios.post('/login', this.state)
      .then(this.onRequestSuccess)
      .catch(this.onRequestError)
  };

  onRequestSuccess = (response) => {
    const cookies = new Cookies();
    cookies.set('jwt', response.data.access_token, { path: '/' });

    if (this.state.remember_me) {
      cookies.set('refresh', response.data.refresh_token, {path: '/'})
    }

    this.props.history.push('/');
  };

  onRequestError = (error) => {
    this.setState({
      errors: error.response.data.message || {},
    });
  };

  render() {
    return (
      <Form>
        <FormGroup>
          <InputError error={this.state.errors.login} />
          <InputGroup>
            <Input
              type="email"
              name="email"
              id="email"
              placeholder="email"
              onChange={this.onFieldChange}
              required
            />
            <InputGroupAddon addonType="append">✉️</InputGroupAddon>
          </InputGroup>
          <InputError error={this.state.errors.email} />
        </FormGroup>
        <FormGroup>
          <InputGroup>
            <Input
              type="password"
              name="password"
              id="password"
              placeholder="password"
              onChange={this.onFieldChange}
              required
            />
            <InputGroupAddon addonType="append">🔑</InputGroupAddon>
          </InputGroup>
          <InputError error={this.state.errors.password} />
        </FormGroup>
        <FormGroup check>
          <Input
            type="checkbox"
            name="remember_me"
            id="remember_me"
            onChange={this.onFieldChange}
          />
          <Label for="remember_me" check>Remember me</Label>
        </FormGroup>
        <Button
          color="primary"
          onClick={this.onClick}
          className="mt-2" block
        >
          Log in
        </Button>
      </Form>
    );
  }
}

export default withRouter(LoginForm);
