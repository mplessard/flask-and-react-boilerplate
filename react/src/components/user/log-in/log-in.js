import React from 'react';

import LoginForm from './form';
import SignUp from './sign-up';

import '../../../static/css/login.scss';
import {Col, Row} from "reactstrap";

export default class LogIn extends React.Component {
  render() {
    return (
      <div id='login'>
        <h1>Login</h1>
        <Row>
        <Col sm="12" md={{size: 6, offset: 3}} className="border-bottom">
          <LoginForm />
          <div className="py-3 text-center">
            <a href="/forgot-password">Forgot password?</a>
          </div>
        </Col>
        </Row>
        <Row>
          <Col className="text-center py-3">
            <SignUp />
          </Col>
        </Row>
      </div>
    );
  }
}