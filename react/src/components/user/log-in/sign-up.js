import React from 'react';

export default class SignUp extends React.Component {
  render() {
    return (
      <div id='sign-up-link'>
        Don’t have an account yet? <a href="/sign-up">Sign up</a>
      </div>
    );
  }
}