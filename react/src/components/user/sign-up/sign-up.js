import React from 'react';

import SignUpForm from './form';
import {Col, Row} from "reactstrap";

export default class SignUp extends React.Component {
  render() {
    return (
      <div id='sign-up'>
        <h1>Sign up</h1>
        <Row>
        <Col sm="12" md={{size: 6, offset: 3}}>
          <SignUpForm />
        </Col>
        </Row>
      </div>
    );
  }
}