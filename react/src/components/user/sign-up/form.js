import React from 'react';
import {
  Form,
  Button,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Alert,
} from 'reactstrap';
import Cookies from 'universal-cookie';
import { withRouter } from 'react-router-dom';
import { InputError } from "../../form/input-error";
import axios from '../../../axiosInstance';

class SignUpForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      alert: {
        isOpen: false,
        message: '',
        color: '',
      },
    }
  }

  componentDidUpdate() {
    if (this.state.alert.color === 'success' && this.state.alert.isOpen) {
      setTimeout(() => this.props.history.push('/'), 1500);
    }
  }

  onFieldChange = (event) => {
    if (event.target.type === 'checkbox') {
      this.setState({
        [event.target.name]: event.target.checked
      });
    } else {
      this.setState({
        [event.target.name]: event.target.value
      });
    }
  };

  areFieldsValid()  {
    if (this.state.email == null) {
      this.setState((prevState) => ({
        errors: {
          ...prevState.errors,
          email: 'Please enter a valid email address.',
        },
        alert: {
          isOpen: true,
          message: 'Oops, something went wrong!',
          color: 'danger',
        },
      }));
      return false;
    }
    return true;
  };

  onClick = () => {
    if (!this.areFieldsValid()) return;

    axios.post('/user', this.state)
      .then(this.onRequestSuccess)
      .catch(this.onRequestError)
  };

  onRequestSuccess = (response) => {
    const cookies = new Cookies();
    cookies.set('jwt', response.data.access_token, { path: '/' });

    this.setState((prevState) => ({
      errors: {},
      alert: {
        isOpen: true,
        message: 'Your account has been successfully created!',
        color: 'success',
      },
    }));
  };

  onRequestError = (error) => {
    this.setState((prevState) => ({
      errors: error.response.data.message || {},
      alert: {
        isOpen: true,
        message: 'Oops, something went wrong!',
        color: 'danger',
      },
    }));
  };

  render() {
    return (
      <React.Fragment>
        <Alert isOpen={this.state.alert.isOpen} color={this.state.alert.color}>
          {this.state.alert.message}
        </Alert>
        <Form>
          <FormGroup>
            <InputGroup>
              <Input
                type="email"
                name="email"
                id="email"
                placeholder="email"
                onChange={this.onFieldChange}
                required
              />
              <InputGroupAddon addonType="append">✉️</InputGroupAddon>
            </InputGroup>
            <InputError error={this.state.errors.email} />
          </FormGroup>
          <FormGroup>
            <InputGroup>
              <Input
                type="password"
                name="password"
                id="password"
                placeholder="password"
                onChange={this.onFieldChange}
                required
              />
              <InputGroupAddon addonType="append">🔑</InputGroupAddon>
            </InputGroup>
            <InputError error={this.state.errors.password} />
          </FormGroup>
          <FormGroup>
            <InputGroup>
              <Input
                type="text"
                name="first_name"
                id="first_name"
                placeholder="first name"
                onChange={this.onFieldChange}
                required
              />
              <InputGroupAddon addonType="append">👤</InputGroupAddon>
            </InputGroup>
            <InputError error={this.state.errors.first_name} />
          </FormGroup>
          <FormGroup>
            <InputGroup>
              <Input
                type="text"
                name="last_name"
                id="last_name"
                placeholder="last name"
                onChange={this.onFieldChange}
                required
              />
              <InputGroupAddon addonType="append">👤</InputGroupAddon>
            </InputGroup>
            <InputError error={this.state.errors.last_name} />
          </FormGroup>
          <Button
            color="primary"
            onClick={this.onClick}
            className="mt-2" block
          >
            Sign up
          </Button>
        </Form>
      </React.Fragment>
    );
  }
}

export default withRouter(SignUpForm);
