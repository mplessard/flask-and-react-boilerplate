import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSeedling } from '@fortawesome/free-solid-svg-icons';

export default class State extends React.PureComponent {
  render() {
    return (
      <div className="text-center text-muted bg-light p-4 rounded">
        <FontAwesomeIcon icon={this.props.icon} size="3x" />
        <p className="font-italic p-4">
          {this.props.message}
        </p>
      </div>
    );
  }
}

State.propTypes = {
  icon: PropTypes.string,
};

State.defaultProps = {
  icon: faSeedling,
};
