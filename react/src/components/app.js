import React, { Component } from 'react';
import { Container } from 'reactstrap';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import '../static/css/app.scss';

import NavBar from './navbar'
import Index from './index';
import LogIn from './user/log-in/log-in';
import SignUp from './user/sign-up/sign-up';
import ForgotPassword from './user/forgot-password/forgot-password';
import Footer from './footer';


export default class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <header className="header">
            <NavBar />
          </header>
          <Container>
            <Route exact path='/' component={Index}/>
            <Route exact path='/log-in' component={LogIn}/>
            <Route exact path='/sign-up' component={SignUp} />
            <Route exact path='/forgot-password' component={ForgotPassword} />
          </Container>
          <Footer />
        </div>
      </Router>
    );
  }
}
