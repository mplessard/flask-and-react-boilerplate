import React from 'react';
import { Collapse } from 'reactstrap';
//import '../static/css/footer.scss';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faBuilding } from '@fortawesome/free-regular-svg-icons';

export default class Footer extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <footer>
        Footer Here
        <Collapse/>
      </footer>
    );
  }
}
